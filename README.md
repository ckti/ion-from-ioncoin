Ion Core integration/staging repository
=====================================

[![Build Status](https://travis-ci.org/cevap/ion.svg?branch=master)](https://travis-ci.org/cevap/ion) [![GitHub version](https://badge.fury.io/gh/cevap%2Fion.svg)](https://badge.fury.io/gh/cevap%2Fion) [![Snap Status](https://build.snapcraft.io/badge/cevap/ion.svg)](https://build.snapcraft.io/user/cevap/ion) [![GitHub issues](https://img.shields.io/github/issues/cevap/ion.svg)](https://bitbucket.org/ioncoin/ion/issues) [![GitHub forks](https://img.shields.io/github/forks/cevap/ion.svg)](https://bitbucket.org/ioncoin/ion/network) [![GitHub stars](https://img.shields.io/github/stars/cevap/ion.svg)](https://bitbucket.org/ioncoin/ion/stargazers) [![GitHub license](https://img.shields.io/github/license/cevap/ion.svg)](https://bitbucket.org/ioncoin/ion) [![Twitter](https://img.shields.io/twitter/url/https/bitbucket.org/ioncoin/ion.svg?style=social)](https://twitter.com/intent/tweet?text=Wow:&url=https%3A%2F%2Fgithub.com%2Fcevap%2Fion)

### Important information

Majority of ion contributors decided to move on from CEVAP's branch to a new one which is believed to fit better to marketing and maybe some other purposes. There will be no further CEVAP releases for ion, project is moved to new community runned repository: https://github.com/ioncoincore/ . Version [v3.1.0](https://bitbucket.org/ioncoin/ion/releases/tag/3.1.0) is the last one which is hosted/published on CEVAP. New repository will be updated in following days and next version will be published only there: 

  https://github.com/ioncoincore/ion/releases
  
For now this new repository is empty and we will move our content in following days. 

  - Project where you can look up the progress of this action: [Move from CEVAP branch](https://github.com/orgs/ioncoincore/projects/1)
  - Download [ioncoincore latest release](https://github.com/ioncoincore/ion/releases)

### Client: Helium

![](assets/images/Helium.jpg)

## Introduction

We released version 3.1.0 as the first stable version of the major upgrade to v3. This release includes a large range of new features, stability improvements and updates, including many contributions from various developers, testers and authors. We thank everybody who contributed and helped to get this release in live status.

### Goals reached

There were several considerations for selecting the current code base.

  - Stop ongoing attacks
  - **Long term goal has been reached**: our source code has been updated to be more closely in line with bitcoin's code base.
  - Improved stability of the core
  - Enhanced interface of the wallet
  - More regular (and secure) difficulaty algorithm (Dark Gravitiy Wave)
  - Good tooling support
 
## Notable changes

Update sources from pivx:
 Last commit: [RPC] Show the configured/set txfee in getwalletinfo
 Tree: https://github.com/PIVX-Project/PIVX/tree/e820cf3816578bbc989ed50cbe4627f4b04e0048
 Commit: https://github.com/PIVX-Project/PIVX/commit/e820cf3816578bbc989ed50cbe4627f4b04e0048

Many thanks to all contributors including following which are not part of our project:
- Aaron Langford
- furszy
- Fuzzbawls
- Jon Spock
- Mrs-X
- Patrick Collins
- PeterL73
- presstab
- rejectedpromise
- sonic
- whateverpal

Here are all changes to version 3.0.2 contributed by users above:
Aaron Langford (2):
      OS-X Build docs was missing a required package
      Application shutdown request interrupts missing accumulators process

```
Fuzzbawls (22):
      v3.0.0 Release
      update gitian aarch64 version
      [Scripts] Fix location for aarch64 outputs in gitian-build.sh
      [Qt] Make lock icon clickable to toggle wallet lock state
      [Qt] Fix UI tab order and shortcuts
      [Qt] Don't allow the Esc key to close the privacy tab
      [Qt] Update Translations
      [Doc] Update release notes for v3.0.1
      [Build] Bump to v3.0.1
      [Build] Bump to v3.0.2
      [Main] Update last checkpoint data
      [Build] Fixup the client version for v3.0.4rc3
      [Build] Add compile/link summary to configure
      [Main] Don't limit zPIV spends from getting into the mempool
      [Qt] Update translations for 3.0.4 final
      [Docs] v3.0.4 Release Notes
      [Main] Avoid slow transaction search with txindex enabled
      Fix 2 instances of mismatched logprint specifiers
      Hook up the logprint-scanner.py script to TravisCI
      [Qt] Periodic make translate
      Update Translations from Transifex for 3.0.6
      Bump Version to 3.0.6

Jon Spock (12):
      Remove unused member wallet in UnlockContext inner class
      Squash reorg_adjustment after removing temp hack
      Change CLIENT_VERSION_BUILD to 99 , IS_RELEASE false
      Masternode quietening commit
      Remove duplicate source code line
      Remove hard-coded GIT_ARCHIVE define
      Fix pivd --help issue using wrong syntax for Parameters
      Combine 2 LogPrintf statement to reduce debug.log clutter
      Missed %d for size in LogPrintf change
      Remove old spork key and enabling logic
      Gitian Hack
      Change git commands called so we can get a better description for non-release source builds

Mrs-X (8):
      Check if model is valid before using in a couple of places in transactionView
      [GUI/RPC] CHanged bubblehelp text + RPC startmasternode help text fixed
      [Core] Don't send not-validated blocks
      [UI] Better error message when too much inputs are used for spending zPIV
      [UI] zPIV-Control: disable negative confirmation numbers.
      [UI] Balance fix + bubble-helps + usability improvements
      [UI] Wallet repair option to resync from scratch
      [UI] Rename SwiftTX -> SwiftX

Patrick Collins (1):
      Fix typo in obfuscation rpc command

PeterL73 (1):
      Fix multiple lines and ignore double %

furszy (1):
      nAccumulatorCheckpoint added to the CBlockHeader copy.

presstab (46):
      Replace deprecated auto_ptr.
      Add -backupzpiv startup flag.
      Fix params order in bip38decrypt.
      Append BIP38 encrypted key with an 4 byte Base58 Checksum
      Fix zPiv spending errors.
      Add checks to see if serial is in the blockchain before considering it already spent.
      Count pending zPiv balance for automint.
      Change sporkDB from smart ptr to ptr.
      Include both pending and mature zerocoins for automint calculations.
      Additional checks for double spending of zPiv serials.
      Add additional checks for txid for zpiv spend.
      New checkpoint block 867733.
      [Build] Bump to v3.0.3
      Refactor zPiv tx counting code. Add a final check in ConnectBlock().
      Make sure init verification is properly checked.
      Only have contextual zPiv checks in ConnectBlock().
      Clear mempool after invalid block from miner.
      Reindex zPiv blocks and correct stats.
      Update checkpoints.
      Better filtering of zPiv serials in miner.
      Refactor accumulators.
      Add memory only accumulator tracking for unit tests.
      Only require 2 mints be added to the accumulator before spending.
      Show the current block in the progress bar.
      -reindexaccumulators startup flag.
      Assert that zerocoinDenomList is size 8.
      Add comments to accumulatormap.cpp.
      Patch libzerocoin. Invalidate spends with bad proofs.
      Post libzerocoin patch consensus adjustments and supply fix.
      [Build] Bump to v3.0.5
      Fix bad operator from 712b18f
      Patch stake miner bug that prevents v4 header version.
      Remove contextual check from CheckZerocoinMint
      Fix runtime errors from tiny format specifiers.
      Bump build to 3.0.5.1
      Adjust testnet chainparams to new hard coded values.
      Do not stringify anything beyond OP_ZEROCOINSPEND for CScript::ToString()
      Fix "accumulator does not verify" when spending zPIV.
      Automate database corruption fix caused by out of sync txdb.
      Remove unused code and add null check.
      Add message popup when db corrupt. Make ReindexAccumulators() use ref list.
      Ask for sporks from 1st peer. Consider Verifying Blocks as IDB.
      Deprecate spork_11.
      Handle debug.log "CWalletTx::GetAmounts: Unknown transaction type" spam.
      Double check tx size when creating zPIV tx's.
      Fix edge case segfault.

rejectedpromise (5):
      [Qt] zPiv control quantity/amount fixes
      Remove unused variables
      protobump
      [QT] Multisignature GUI
      [Main] Allow for zPIV maintenance mode.

sonic (1):
      Added script that scans codebase for LogPrint(f) statements that do not have the correct number of arguments.

whateverpal (1):
      Add getspentzerocoinamount RPC call
```

### Our sources: 
We forked from [ION](https://bitbucket.org/ioncoin/ion) and integrated ION's specific features into the IonX codebase.

By doing so, we connect to an enthusiastic and active community - leaving behind old Ion code that inherits from less actively developed and maintaned code. Our main sources are now derived from:

  1. [ION](https://bitbucket.org/ioncoin/ion)
  1. [DASH](https://github.com/dashpay/dash)
  1. [Bitcoin](https://github.com/bitcoin/bitcoin)


More information at [ioncore.xyz](https://www.ioncore.xyz) Visit our ANN thread at [BitcoinTalk](https://bitcointalk.org/index.php?topic=1443633.7200)

### Coin Specs

|         **Coin Specifications**          |
|-----------------------|-----------------:|
|       Algorithm       |      Quark       |
| Retargeting Algorithm |       DGW        |
|      Block Time       |    60 Seconds    |
|    Max Coin Supply    | 48,035,935.4 ION |
|        Premine        |  16,400,000 ION* |

*16,629,951 Ion Premine was burned in block [1](https://chainz.cryptoid.info/ion/block.dws?000000ed2f68cd6c7935831cc1d473da7c6decdb87e8b5dba0afff0b00002690.htm)

### Reward Distribution

|                    **Genesis Block**                     |
|------------------|:------------------:|-----------------:|
| **Block Height** |  **Reward Amount** |    **Notes**     |
|        1         |   16,400,000 ION   | Initial Pre-mine |


### PoW Rewards Breakdown

| **Block Height** |  **Masternodes** |   **Miner**    |   **Total**     |
|------------------|:----------------:|---------------:|----------------:|
|      2-454       |  50% (11.5 ION)  | 50% (11.5 ION) |      10,419 ION |


### PoS/PoW Rewards Breakdown

| **Block Height** |  **Masternodes** |   **Miner**    |   **Total**     |
|------------------|:----------------:|---------------:|----------------:|
|     455-1000     |  50% (11.5 ION)  | 50% (11.5 ION) |      12,558 ION |

### PoS Rewards Breakdown

| **Block Height** |  **Masternodes** |   **Miner**    |    **Total**    |
|   1001-125147    |  50% (11.5 ION)  | 50% (11.5 ION) |   2,855,381 ION |
|  125148-550001   |  50% (8.5 ION)   | 50% (11.5 ION) |   2,855,381 ION |
|  550002-551441   |  50% (0.01 ION)  | 50% (11.5 ION) |   7,222,518 ION |
|  551442-570063   |  50% (8.5 ION)   | 50% (11.5 ION) |        28.8 ION |
|  570064-1013539  |  50% (5.75 ION)  | 50% (11.5 ION) |     316,574 ION |
| 1013540-1457015  |  50% (2.875 ION) | 50% (11.5 ION) |   2,549,987 ION |
| 1457016-3677391  |  50% (0.925 ION) | 50% (11.5 ION) | 4,107,695.6 ION |
| 3677392-50981391 |  50% (0.1 ION)   | 50% (11.5 ION) |   9,460,800 ION |
